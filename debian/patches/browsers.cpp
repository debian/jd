modify browser list for Debian (Debian specific patch)
Index: jd-2.8.5~rc120811/src/browsers.cpp
===================================================================
--- jd-2.8.5~rc120811.orig/src/browsers.cpp	2012-02-11 19:48:43.000000000 +0900
+++ jd-2.8.5~rc120811/src/browsers.cpp	2012-08-13 23:08:35.067499922 +0900
@@ -6,7 +6,7 @@
 enum
 {
     MAX_TEXT = 256,
-    BROWSER_NUM = 8,
+    BROWSER_NUM = 10,
 #ifdef _WIN32
     MAX_SAFE_PATH = 1024,
 #endif
@@ -26,6 +26,8 @@
         { "chrome (Google pack)",     "\"C:/Program Files/Google/Chrome/Application/chrome.exe\" \"%LINK\"" }
 #else
         { "標準ブラウザ(xdg-open)",   "xdg-open \"%LINK\"" },
+        { "Debian Sensible ブラウザ (システム設定依存)", "sensible-browser \"%LINK\"" },
+        { "iceweasel", "iceweasel \"%LINK\"" },
         { "Firefox",                  "firefox \"%LINK\"" },
         { "konqeror",                 "konqeror \"%LINK\"" },
         { "opera 9.* 以降",           "opera -remote \"openURL(%LINK,new-tab)\"" },
